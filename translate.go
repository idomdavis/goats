package goats

import "sort"

//nolint:gochecknoglobals // don't want to have to create an instance of goats.
var translators = []Translator{Go, Java}

// Register a translator with goats.
func Register(translator Translator) {
	translators = append(translators, translator)
}

// Translate a string using the highest matching translator. If no translators
// provide a match then the string is returned as is.
func Translate(input string) string {
	translations := make([]*Translation, len(translators))

	for i, t := range translators {
		translations[i] = t(input)
	}

	sort.Slice(translations, func(i, j int) bool {
		return translations[i].Confidence > translations[j].Confidence
	})

	if translations[0].Confidence == 0 {
		return input
	}

	return translations[0].Format
}

// Reset the translator list to the default.
func Reset() {
	translators = []Translator{Go, Java}
}
