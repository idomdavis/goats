# Go Alternative Time Strings (goats)

[![Build](https://img.shields.io/bitbucket/pipelines/idomdavis/goats/main?style=plastic)](https://bitbucket.org/idomdavis/goats/addon/pipelines/home)
[![Issues](https://img.shields.io/bitbucket/issues-raw/idomdavis/goats?style=plastic)](https://bitbucket.org/idomdavis/goats/issues)
[![Pull Requests](https://img.shields.io/bitbucket/pr-raw/idomdavis/goats?style=plastic)](https://bitbucket.org/idomdavis/goats/pull-requests/)
[![Go Doc](http://img.shields.io/badge/godoc-reference-5272B4.svg?style=plastic)](http://godoc.org/github.com/idomdavis/goats)
[![License](https://img.shields.io/badge/license-MIT-green?style=plastic)](https://opensource.org/licenses/MIT)


![A Goat eating string wrapped around a somewhat alternative clock](goats.png)

Go Alternative Time Strings provides mappings between other time formats, and 
the [Go][go-time] time format. The library will attempt to determine what 
format the time string is in based on the number of matching format patterns.

For example, consider the following Go and Java format patterns:

```
goats.Translate("2006-01-02T15:04:05")
goats.Translate("yyyy-MM-dd'T'HH:mm:ss"")
```

Both yield a format string of `2006-01-02T15:04:05`.

New formatters can be registered with Go Alternative Time Strings allowing
arbitrary time formatting strings to be used.

[go-time]: https://golang.org/pkg/time/#example_Time_Format

## Attribution

Goats logo by [ACID](https://www.andycarolan.co.uk).

## Copyright

The goats image is Copyright (c) 2021 Dom Davis and may only be used in relation
to this library.
