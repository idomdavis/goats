package goats_test

import (
	"testing"
	"time"

	"bitbucket.org/idomdavis/goats"
)

func TestJava(t *testing.T) {
	t.Parallel()

	t.Run("Single digit hours should work", func(t *testing.T) {
		t.Parallel()

		f := goats.Translate("d/M/yy h:mm")
		d, err := time.Parse(f, "4/4/91 0:00")

		expect := time.Date(1991, time.April, 4, 0, 0, 0, 0, time.UTC)

		switch {
		case err != nil:
			t.Errorf("Unexpected error parsing Java format: %v", err)
		case !d.Equal(expect):
			t.Errorf("Unexpected date, got %s expect %s", d.String(), expect.String())
		}
	})
}
