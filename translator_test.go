package goats_test

import (
	"fmt"
	"regexp"

	"bitbucket.org/idomdavis/goats"
)

func ExampleTranslation_Strip() {
	format := "'Time' HH:mm"

	t := &goats.Translation{Format: format}
	t.Strip(regexp.MustCompile(`'(?:[^']|'')*'`), "@")

	fmt.Println(t.Format)

	// Output:
	// @ HH:mm
}

func ExampleTranslation_Rebuild() {
	format := "'Time' HH:mm"

	t := &goats.Translation{Format: format}
	t.Strip(regexp.MustCompile(`'(?:[^']|'')*'`), "@")
	t.Rebuild(regexp.MustCompile(`^'|'$`))

	fmt.Println(t.Format)

	// Output:
	// Time HH:mm
}

func ExampleTranslation_Update() {
	format := "'Time' HH:mm"

	t := &goats.Translation{Format: format}
	t.Update(regexp.MustCompile(`'.*' `), "")

	fmt.Println(t.Confidence, t.Format)

	// Output:
	// 0 HH:mm
}

func ExampleTranslation_Match() {
	format := "'Time' HH:mm"

	t := &goats.Translation{Format: format}
	t.Match("HH")

	fmt.Println(t.Confidence, t.Format)

	// Output:
	// 1 'Time' HH:mm
}

func ExampleTranslation_Replace() {
	format := "'Time' HH:mm"

	t := &goats.Translation{Format: format}
	t.Replace("'Time' ", "")

	fmt.Println(t.Confidence, t.Format)

	// Output:
	// 1 HH:mm
}
