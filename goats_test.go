package goats_test

import (
	"fmt"
	"time"

	"bitbucket.org/idomdavis/goats"
)

func Example() {
	t := time.Date(2015, time.March, 12, 15, 6, 0, 0, time.UTC)

	fmt.Println(t.Format(goats.Translate("2006-01-02T15:04:05")))
	fmt.Println(t.Format(goats.Translate("Mon, 2 Jan 2006 15:04:05 MST")))
	fmt.Println(t.Format(goats.Translate("yyyy-MM-dd'T'HH:mm:ss")))
	fmt.Println(t.Format(goats.Translate("EEE, d MMM yyyy HH:mm:ss z")))

	// Output:
	// 2015-03-12T15:06:00
	// Thu, 12 Mar 2015 15:06:00 UTC
	// 2015-03-12T15:06:00
	// Thu, 12 Mar 2015 15:06:00 UTC
}
