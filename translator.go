package goats

import (
	"regexp"
	"strings"
)

// Translator function that will take a source time format and return a
// Translation.
type Translator func(source string) *Translation

// Translation of a time format.
type Translation struct {
	// Confidence in the translation. Confidence is set to the number of
	// matching formatting patterns that were found.
	Confidence int

	// Format string for the translation.
	Format string

	placeholder  string
	placeholders []string
}

// Strip and store literals from the format string. This allows Update, Match
// Replace to ignore string literals during translation. Use Rebuild to return
// the literals to the Format.
func (t *Translation) Strip(literal *regexp.Regexp, placeholder string) {
	t.placeholders = literal.FindAllString(t.Format, -1)
	t.placeholder = placeholder

	t.Format = literal.ReplaceAllString(t.Format, placeholder)
}

// Update the Format by finding and replacing all instances of the find regexp
// with replace. The Confidence is not affected by Update.
func (t *Translation) Update(find *regexp.Regexp, replace string) {
	t.Format = find.ReplaceAllString(t.Format, replace)
}

// Match the given string and update the Confidence with the number of matches.
// Returns true if any matches were found, false otherwise.
func (t *Translation) Match(s string) bool {
	var confidence int

	if s != "" {
		confidence = strings.Count(t.Format, s)
	}

	t.Confidence += confidence

	return confidence > 0
}

// Replace source with target and update the Confidence with the number of
// matches. Returns true if any replacements were made, false otherwise.
func (t *Translation) Replace(source, target string) bool {
	match := t.Match(source)

	if match {
		t.Format = strings.ReplaceAll(t.Format, source, target)
	}

	return match
}

// Rebuild the format with the literals stripped with Strip. The literals have
// any string that matches string replaced with the empty string before being
// added back into the format.
func (t *Translation) Rebuild(strip *regexp.Regexp) {
	for _, p := range t.placeholders {
		p = strip.ReplaceAllString(p, "")
		t.Format = strings.Replace(t.Format, t.placeholder, p, 1)
	}
}
