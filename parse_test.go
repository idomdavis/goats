package goats_test

import (
	"fmt"
	"strings"
	"testing"

	"bitbucket.org/idomdavis/goats"
)

func ExampleParse() {
	fmt.Println(goats.Parse("1900", "44619"))
	fmt.Println(goats.Parse("excel", "40729"))
	fmt.Println(goats.Parse("excel", "40729.74653"))
	fmt.Println(goats.Parse("1904", "39267"))
	fmt.Println(goats.Parse("yyyy/MM/dd z", "2022/02/14 UTC"))

	// Output:
	// 2022-02-27 00:00:00 +0000 UTC <nil>
	// 2011-07-05 00:00:00 +0000 UTC <nil>
	// 2011-07-05 00:00:00 +0000 UTC <nil>
	// 2011-07-05 00:00:00 +0000 UTC <nil>
	// 2022-02-14 00:00:00 +0000 UTC <nil>
}

func TestParse(t *testing.T) {
	t.Parallel()

	t.Run("Days will error if non numeric", func(t *testing.T) {
		t.Parallel()

		d, err := goats.Parse("excel", "Jan")

		switch {
		case err == nil:
			t.Error("Expected error")
		case !strings.HasPrefix(err.Error(), "goats.Parse"):
			t.Errorf("Unexpected error: %v", err)
		case !d.IsZero():
			t.Errorf("Expected zero date, got %s", d.String())
		}
	})

	t.Run("Parse errors will propagate", func(t *testing.T) {
		t.Parallel()

		d, err := goats.Parse("M", "foo")

		switch {
		case err == nil:
			t.Error("Expected error")
		case !strings.HasPrefix(err.Error(), "goats.Parse"):
			t.Errorf("Unexpected error: %v", err)
		case !d.IsZero():
			t.Errorf("Expected zero date, got %s", d.String())
		}
	})
}
