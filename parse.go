package goats

import (
	"fmt"
	"math"
	"strconv"
	"time"
)

const (
	since1900 = 1900
	since1904 = 1904
)

// Parse a date using the given layout string. The layout string will be
// translated if possible and then the Go version of the string passed to
// time.Parse. If the layout is "days" or "excel" then Parse will take this as
// days since 1900.
func Parse(layout, value string) (time.Time, error) {
	var (
		date time.Time
		err  error
	)

	switch layout {
	case "excel", "1900":
		date, err = excel(value, since1900)
	case "1904":
		date, err = excel(value, since1904)
	default:
		date, err = time.Parse(Translate(layout), value)
	}

	if err != nil {
		err = fmt.Errorf("goats.Parse(%q, %q): %w", layout, value, err)
	}

	return date, err
}

func excel(value string, year int) (time.Time, error) {
	d, err := strconv.ParseFloat(value, 64)

	if err != nil {
		return time.Time{}, fmt.Errorf("invalid value for days since: %w", err)
	}

	days := int(math.Floor(d))
	epoch := time.Date(year, time.January, 1, 0, 0, 0, 0, time.UTC)

	if year == since1900 {
		// Jan 1, 1900 is day 1 (not the number of days since Jan 1, 1900) so
		// start the day before.
		days--

		// According to Excel Feb 29, 1900 exists. So lets get rid of that.
		const buggy = 31 + 28 // Days in Jan + Feb.

		if days >= buggy {
			days--
		}
	}

	return epoch.AddDate(0, 0, days), nil
}
