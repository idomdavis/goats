package goats

import "regexp"

// Java will provide a Translation from Java date time format. Both DD and D
// will produce space padded day of year. Both HH and H will produce a zero
// padded 24 hour representation of the hour. All zero padded patterns are
// reduced to the shorted possible number of zeros, except for S which is 0
// padded to the number of S's provided. z is a standard timezone (MST), Z
// returns a numeric timezone (-0700) and z returns a numeric timezone with a
// colon (-07:00).
//
//nolint:funlen,cyclop // Better to keep the logic in one place since its all similar.
func Java(source string) *Translation {
	t := &Translation{Format: source}
	t.Strip(regexp.MustCompile(`'(?:[^']|'')*'`), "@")
	t.Update(regexp.MustCompile(`a`), "A")
	t.Update(regexp.MustCompile(`M{5,}`), "MMMM")

	switch {
	case t.Replace("MMMM", "January"):
	case t.Replace("MMM", "Jan"):
	case t.Replace("MM", "01"):
	case t.Replace("M", "1"):
	}

	t.Update(regexp.MustCompile(`E{5,}`), "EEEE")
	t.Update(regexp.MustCompile(`E{2,3}`), "E")

	switch {
	case t.Replace("EEEE", "Monday"):
	case t.Replace("E", "Mon"):
	}

	t.Update(regexp.MustCompile(`d{3,}`), "dd")

	switch {
	case t.Replace("dd", "02"):
	case t.Replace("d", "2"):
	}

	t.Update(regexp.MustCompile(`D{4,}`), "DDD")

	switch {
	case t.Replace("DDD", "002"):
	case t.Replace("DD", "__2"):
	case t.Replace("D", "__2"):
	}

	t.Update(regexp.MustCompile(`h{3,}`), "hh")
	t.Update(regexp.MustCompile(`H{3,}`), "HH")

	switch {
	case t.Replace("HH", "15"):
	case t.Replace("H", "15"):
	case t.Replace("hh", "03"):
	case t.Replace("h", "3"):
	}

	t.Update(regexp.MustCompile(`m{3,}`), "mm")

	switch {
	case t.Replace("mm", "04"):
	case t.Replace("m", "4"):
	}

	t.Update(regexp.MustCompile(`s{3,}`), "ss")

	switch {
	case t.Replace("ss", "05"):
	case t.Replace("s", "5"):
	}

	t.Update(regexp.MustCompile(`y{3,}`), "yyyy")

	switch {
	case t.Replace("yyyy", "2006"):
	case t.Replace("yy", "06"):
	}

	t.Replace("A", "PM")

	t.Update(regexp.MustCompile(`([^S])S`), "$1.S")
	t.Replace("S", "0")

	switch {
	case t.Replace("z", "MST"):
	case t.Replace("Z", "-0700"):
	case t.Replace("X", "-07:00"):
	}

	t.Rebuild(regexp.MustCompile(`^'|'$`))

	return t
}
