package goats_test

import (
	"fmt"
	"math"

	"bitbucket.org/idomdavis/goats"
)

func ExampleRegister() {
	fmt.Printf("%q\n", goats.Translate(""))

	goats.Register(func(input string) *goats.Translation {
		return &goats.Translation{Format: "translated", Confidence: math.MaxInt64}
	})

	fmt.Printf("%q\n", goats.Translate(""))

	goats.Reset()

	// Output:
	// ""
	// "translated"
}
