package goats

// Go translator does nothing to the source string, but set a confidence based
// on the number of matches to the Go time format string in the source.
//
//nolint:cyclop // Better to keep the logic in one place since its all similar.
func Go(source string) *Translation {
	t := &Translation{Format: source}

	switch {
	case t.Match("January"):
	case t.Match("Jan"):
	case t.Match("01"):
	case t.Match("1"):
	}

	switch {
	case t.Match("Monday"):
	case t.Match("Mon"):
	}

	switch {
	case t.Match("02"):
	case t.Match("2"):
	}

	switch {
	case t.Match("002"):
	case t.Match("__2"):
	}

	switch {
	case t.Match("15"):
	case t.Match("03"):
	case t.Match("3"):
	}

	switch {
	case t.Match("04"):
	case t.Match("4"):
	}

	switch {
	case t.Match("05"):
	case t.Match("5"):
	}

	switch {
	case t.Match("2006"):
	case t.Match("06"):
	}

	switch {
	case t.Match("MST"):
	case t.Match("-0700"):
	case t.Match("-07:00"):
	}

	return t
}
